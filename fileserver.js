var http = require("http");
var url = require("url");
var path = require("path");
var fs = require("fs");

var mimeType = {
    '.css': 'text/css',
    '.html': 'text/html',
    '.js': 'text/javscript',
    '.png': 'image/png',
    '.jpg': 'image/jpg'
};

var server = http.createServer(function (request, response) {
    var requestedFile = url.parse(request.url).pathname;
    var filename = path.join(process.cwd(), requestedFile);

    fs.exists(filename, function(exists) {
        if (! exists) {
            response.writeHead(404,
                {"Content-Type": mimeType || 'text/plain'});
            response.write("<h1>404 Error</h1>\n");
            response.write("The requested file isn't on this machine\n");
            response.end();
            return;
        }

        if (fs.statSync(filename).isDirectory())
            filename += '/TravelBlog-Home.html';

        fs.readFile(filename, "binary", function(err, file) {
            if (err) {
                response.writeHead(500, {'Content-Type': mimeType || 'text/plain'});
                response.write("<h1>500 Error</h1>\n");
                response.write(err + "\n");
                response.end();
                return;
            }
            response.writeHead(200);
            response.write(file, "binary");
            response.end();
        });
    });
});
server.listen(7000, "localhost");
console.log("Server is running at http://127.0.0.1:7000/");